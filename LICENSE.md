Copyright <2023> <BLURT COMMUNITY>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

---

NB: Blurt is a fork of steemit, and is a graphene consensus blockchain. Graphene was developed by Dan Larimer and his team for BitShares, and later used by Dan and others to create Steemit. Blurt's previous license made the mistake of attemting to restrict the use of some unique code in Blurt developed by Tuan Pham Anh, namely, the world's first snapshot-airdrop.

Ultimately blurt contains the contributions of countless individuals, and we are grateful to each of them.

Please use the blurt code in any way that you see fit, without restriction.

Don't steal from users like Steemit and Hive did.
