const config = require('config.json')('./config/config.json')
const express = require('express')
const axios = require('axios')
const winston = require('winston')
const expressWinston = require('express-winston')
const multer = require('multer')
const createError = require('http-errors')
const cors = require('cors')
const chainLib = require('@blurtfoundation/blurtjs')
const { createHash } = require('crypto')
const {
  PublicKey,
  Signature
} = require('@blurtfoundation/blurtjs/lib/auth/ecc')
const RIPEMD160 = require('ripemd160')
const AWS = require('aws-sdk')
const { RateLimiterMemory } = require('rate-limiter-flexible')

chainLib.api.setOptions({
  url: config.blurt_rpc_node,
  alternative_api_endpoints: config.alternative_blurt_rpc_nodes,
  retry: true,
  useAppbaseApi: true
})

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: { service: 'user-service' },
  transports: [
    new winston.transports.Console({
      format: winston.format.simple()
    })
  ]
})

const must = (fn) => (req, res, next) => {
  Promise.resolve(fn(req, res, next)).catch(next)
}

const acceptedMimeTypes = [
  'image/gif',
  'image/jpeg',
  'image/png',
  'image/webp'
]

const uploadFileFilter = (req, file, cb) => {
  if (acceptedMimeTypes.includes(file.mimetype)) {
    cb(null, true)
  } else {
    cb(
      createError(
        400,
        'Invalid file type; allowed types are: gif, jpeg, png, webp.'
      ),
      false
    )
  }
}

const storage = multer.memoryStorage()

const upload = multer({
  storage,
  fileFilter: uploadFileFilter,
  limits: {
    files: 1,
    fileSize: config.max_image_size_in_bytes
  }
})

const s3 = new AWS.S3({
  accessKeyId: config.s3_config.access_key_id,
  secretAccessKey: config.s3_config.secret_access_key,
  region: config.s3_config.region,
  endpoint: config.s3_config.endpoint,
  signatureVersion: 'v4'
})

const rate_limit_opts = {
  points: config.rate_limit_points, // number of images
  duration: 3600 // per hour
}
const rateLimiter = new RateLimiterMemory(rate_limit_opts)

const app = express()
app.disable('x-powered-by')
const port = config.port || 7000 // set our port

const errorHandler = async (err, req, res, next) => {
  if (res.headersSent) {
    return next(err)
  }

  if (err instanceof multer.MulterError) {
    switch (err.code) {
      case 'LIMIT_UNEXPECTED_FILE':
        res.status(400).json({
          error: `Unexpected field name ${err.field}; field name must be 'file'.`
        })
        return
      case 'LIMIT_FILE_COUNT':
        res.status(400).json({ error: 'Too many files; limit is 1.' })
        return
      case 'LIMIT_FILE_SIZE':
        res.status(413).json({
          error: `File too large; limit is ${
            config.max_image_size_in_bytes / 1024 / 1024
          } MB.`
        })
        return
      default:
        res.status(400).json({ error: 'Invalid form data!' })
        return
    }
  }

  if (createError.isHttpError(err) && err.status < 500) {
    res.status(err.status).json({ error: err.message })
    return
  }
  res.status(500).json({ error: 'Internal Server Error!' })
}

const healthCheck = async (req, res) => {
  res.set('Cache-Control', 'no-cache')
  res.status(200).json({ ok: true, version: '1.0.0', date: new Date() })
}

let coal = {}
const retrieveCOAL = async () => {
  await axios
    .get(config.coal.url, { timeout: config.coal.timeout })
    .then((response) => {
      const map = new Map()
      if (response.status === 200) {
        // eslint-disable-next-line no-restricted-syntax
        for (const data of response.data) {
          map.set(data.name, data)
        }
        coal = map
        logger.info(`COAL loaded with ${coal.size} users.`)
      }
    })
    .catch((error) => {
      console.error(error)
    })
}

const hdl_upload_s3 = async (req, res, next) => {
  const { username, sig } = req.params

  if (username === undefined || username === null) {
    return next(createError(404, 'Invalid username.'))
  }

  const isNotValidUsername = chainLib.utils.validateAccountName(username)
  if (isNotValidUsername) {
    return next(createError(404, 'Invalid username.'))
  }

  const existingAccs = await chainLib.api.getAccountsAsync([username])
  if (existingAccs.length !== 1) {
    return next(createError(404, 'User does not exist.'))
  }

  if (coal.get(username)) {
    return next(
      createError(
        403,
        `Uploading blocked; @${username} is in COAL (Collaboratively Organized Abuse List).`
      )
    )
  }

  if (req.file === undefined || req.file === null) {
    return next(createError(400, 'File missing.'))
  }

  await rateLimiter
    .consume(req.params.username)
    .then((rateLimiterRes) => {
      res.set('X-RateLimit-Remaining', rateLimiterRes.remainingPoints)
      res.set(
        'X-RateLimit-Reset',
        new Date(Date.now() + rateLimiterRes.msBeforeNext)
      )
    })
    .catch((rateLimiterRes) => {
      res.set('X-RateLimit-Remaining', rateLimiterRes.remainingPoints)
      res.set(
        'X-RateLimit-Reset',
        new Date(Date.now() + rateLimiterRes.msBeforeNext)
      )
      return next(
        createError(
          429,
          `Upload quota exceded; You're allowed ${rate_limit_opts.points} images per hour.`
        )
      )
    })

  if (req.file === undefined || req.file === null) {
    return next(createError(400, 'File missing.'))
  }
  const content_type = req.file.mimetype
  const split_file_name = req.file.originalname.split('.')
  const file_ext = split_file_name[split_file_name.length - 1]
  const hash_buffer = new RIPEMD160().update(req.file.buffer).digest('hex')
  const s3_file_path = `${username}/${hash_buffer}.${file_ext}`

  // verifying sig

  const sign_data = Signature.fromBuffer(Buffer.from(sig, 'hex'))

  const imageHash = createHash('sha256')
    .update('ImageSigningChallenge')
    .update(req.file.buffer)
    .digest()

  const sigPubKey = sign_data.recoverPublicKey(imageHash).toString()

  const postingPubKey = existingAccs[0].posting.key_auths[0][0]
  const activePubKey = existingAccs[0].active.key_auths[0][0]
  const ownerPubKey = existingAccs[0].owner.key_auths[0][0]

  switch (sigPubKey) {
    case postingPubKey:
    case activePubKey:
    case ownerPubKey:
      // key matched, do nothing
      break
    default:
      return next(createError(400, 'Invalid signing key.'))
  }

  const is_verified = sign_data.verifyHash(
    imageHash,
    PublicKey.fromString(sigPubKey)
  )
  if (!is_verified) {
    return next(createError(400, 'Invalid signature.'))
  }

  await s3
    .putObject({
      ACL: 'public-read',
      Bucket: config.s3_config.bucket,
      Key: s3_file_path,
      Body: req.file.buffer,
      ContentType: content_type
    })
    .promise()

  const img_full_path = `${config.prefix_url}${config.s3_config.bucket}/${s3_file_path}`
  res.status(200).json({
    ok: true,
    message: 'Successfully uploaded image.',
    url: img_full_path
  })
}

serverStart = () => {
  app.use(cors())
  const router = express.Router()
  router.post(
    '/:username/:sig',
    must(upload.single('file')),
    must(hdl_upload_s3)
  )
  router.get('/', must(healthCheck))
  router.get('/test_cors', must(healthCheck))

  app.use(
    expressWinston.logger({
      transports: [new winston.transports.Console()],
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.simple()
      ),
      meta: false,
      expressFormat: true,
      colorize: true
    })
  )

  app.use(router)

  app.use(function (req, res, next) {
    return next(createError(404, 'Invalid route!'))
  })

  app.use(
    expressWinston.errorLogger({
      transports: [new winston.transports.Console()],
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.simple()
      ),
      expressFormat: true,
      colorize: true
    })
  )

  app.use(errorHandler)

  app.listen(port)
  console.log(`serverStart on port ${port}`)
}

serverStart()
retrieveCOAL()
setInterval(retrieveCOAL, config.coal.interval)
