import { normalize } from './uri_normalize'
import {
  getJsonRpcUri,
  request_remote_image,
  accepted_content_types,
  emptyObject
} from './utils'
import { redisClient } from './redisClient'
const config = require('config.json')('./config/config.json')
const express = require('express')
const path = require('path')
const chainLib = require('@blurtfoundation/blurtjs')
const sharp = require('sharp')

const router = express.Router()

const get_image_url = async (accountname) => {
  let url = null

  try {
    const [acc] = await getJsonRpcUri('condenser_api', 'get_accounts', [
      [accountname]
    ])

    if (acc) {
      const json_metadata = acc.json_metadata
      const md = JSON.parse(json_metadata)

      if (md.profile.profile_image) {
        url = md.profile.profile_image
      }

      // console.log("url=" + url);
      if (typeof url === 'undefined' || url === null) {
        url = null
      }
    }
  } catch (e) {
    // console.log(e.message);
    url = null
  }

  return url
}

router.get('/:accountname/:size?', async function (req, res) {
  try {
    let img
    let metadata
    let content_type
    let img_size = 64 // default is 64
    const { accountname, size } = req.params

    const isValidUsername = chainLib.utils.validateAccountName(accountname)
    if (isValidUsername) {
      throw new Error(isValidUsername)
    }

    if (size === '32x32') {
      img_size = 32
    } else if (size === '48x48') {
      img_size = 48
    } else if (size === '64x64') {
      img_size = 64
    } else if (size === '96x96') {
      img_size = 96
    } else if (size === '128x128') {
      img_size = 128
    } else if (size === '256x256') {
      img_size = 256
    } else if (size === '512x512') {
      img_size = 512
    }

    const img_key = `profileImage-${accountname}-${img_size}`

    const redisRes = await redisClient.hgetallBuffer(img_key)

    if (!emptyObject(redisRes)) {
      console.log(`Serving Cached Profile Image for ${accountname}`)
      img = redisRes.imgData
      content_type = redisRes.contentType
    } else {
      let url = await get_image_url(accountname)

      if (typeof url === 'undefined' || url === null) {
        throw new Error(`no profile image for ${accountname}`)
      }

      url = normalize(url)
      if (!url) {
        throw new Error('invalid url')
      }

      // temporary workaround because imgur is rate-limiting our proxy
      if (
        url.startsWith('https://i.imgur.com/') ||
        url.startsWith('http://i.imgur.com/') ||
        url.startsWith('https://imgur.com/') ||
        url.startsWith('http://imgur.com/')
      ) {
        console.log('Redirecting to imgur - URL:', url)
        res.redirect(url)
        return
      }

      const img_res = await request_remote_image(url)
      let status_code = Math.floor(img_res.statusCode / 100)

      // handle redirecting
      if (status_code === 3) {
        url = img_res.headers.location
        url = normalize(url)
        if (!url) {
          throw new Error('invalid url')
        }

        img_res = await request_remote_image(url)
        status_code = Math.floor(img_res.statusCode / 100)
      }

      if (status_code !== 2) {
        if (img_res.statusCode === 429) {
          console.log('Headers:', img_res.headers)
          console.log('Body:', img_res.body)
        }
        throw new Error(`error on remote response (${img_res.statusCode})`)
      }

      const content_length = img_res.headers['content-length']
      if (content_length > config.max_size) {
        throw new Error(`Resource size exceeds limit (${content_length})`)
      }

      img = img_res.body

      metadata = await sharp(img, { failOnError: true }).metadata()
      img = await sharp(img, { failOnError: true })
        .resize(img_size, img_size)
        .toBuffer()

      content_type = img_res.headers['content-type']
      if (!accepted_content_types.includes(content_type)) {
        throw new Error(`Unsupported content-type (${content_type})`)
      }
      if (
        content_type === 'binary/octet-stream' ||
        content_type === 'application/octet-stream'
      ) {
        content_type = `image/${metadata.format}`
      }

      await redisClient
        .multi()
        .hset(img_key, 'contentType', content_type)
        .hset(img_key, 'imgData', img)
        .expire(img_key, 600) // keep the image cached in Redis for 10 minutes.
        .exec()
    }

    if (content_type) {
      res.set('content-type', content_type)
    }

    res.set('Cache-Control', 'public,max-age=86400,immutable')

    res.end(img, 'binary')
  } catch (e) {
    console.log(e.message)

    res.set('content-type', 'image/png')
    res.set('Cache-Control', 'public,max-age=600,immutable') // cache 10 min
    res.sendFile('user.png', { root: path.join(__dirname, '../assets') })
  }
})

module.exports = router
