import { normalize } from './uri_normalize'
import {
  request_remote_image,
  accepted_content_types,
  imageBlacklist,
  loadImageBlacklist,
  emptyObject
} from './utils'
import { redisClient } from './redisClient'
const config = require('config.json')('./config/config.json')
const express = require('express')
const path = require('path')
const sharp = require('sharp')
const needle = require('needle')
const queryString = require('query-string')

require('tls').DEFAULT_ECDH_CURVE = 'auto'

const assetsPath = path.join(__dirname, '../assets')

loadImageBlacklist()

const router = express.Router()

// http://sharp.dimens.io/en/stable/api-utility/#cache
sharp.cache(false)
sharp.concurrency(1)

router.get('/:width(\\d+)x:height(\\d+)/*?', async (req, res, next) => {
  try {
    let { width, height } = req.params
    let url = req.params[0]

    if (width) {
      width = parseInt(width)
      if (width > 1920) {
        width = 1920
      }
    } else {
      width = 0
    }

    if (height) {
      height = parseInt(height)
      if (height > 2160) {
        height = 2160
      }
    } else {
      height = 0
    }

    if (url === undefined) {
      throw new Error('invalid url')
    }

    /**
     * fix for protocol-relative URL, eg.,
     * //futuristictoday.com/wp-content/uploads/2017/12/Robots-to-revolutionize-farming-ease-labor-woes.jpg
     */
    if (url.startsWith('//')) {
      url = `https:${url}`
    }

    // fix to get query string
    const query_str = queryString.stringify(req.query)
    if (query_str.length > 0) {
      url = `${url}?${query_str}`
    }

    url = normalize(url)
    if (!url) {
      throw new Error('invalid url')
    }

    // temporary workaround because imgur is rate-limiting our proxy
    if (
      url.startsWith('https://i.imgur.com/') ||
      url.startsWith('http://i.imgur.com/') ||
      url.startsWith('https://imgur.com/') ||
      url.startsWith('http://imgur.com/')
    ) {
      console.log('Redirecting to imgur - URL:', url)
      res.redirect(url)
      return
    }

    if (imageBlacklist.includes(url)) {
      throw new Error('Blacklisted image')
    }

    let bypassCache = false
    if (
      config.image_server_host !== '' &&
      url.search(config.image_server_host) > -1
    ) {
      bypassCache = true
    }

    let redisRes = {}
    let img
    let content_type
    const img_key = `${width}X${height} - ${url}`
    if (!bypassCache) {
      redisRes = await redisClient.hgetallBuffer(img_key)
    }

    if (!bypassCache && !emptyObject(redisRes)) {
      console.log('Serving Cached Image - URL:', url)
      content_type = redisRes.contentType
      img = redisRes.imgData
    } else {
      let img_res = await request_remote_image(url)

      let status_code = Math.floor(img_res.statusCode / 100)

      // handle redirecting
      if (status_code === 3) {
        url = img_res.headers.location
        url = normalize(url)
        if (!url) {
          throw new Error('invalid url')
        }

        img_res = await request_remote_image(url)
        status_code = Math.floor(img_res.statusCode / 100)
      }

      if (status_code !== 2) {
        if (img_res.statusCode === 429) {
          console.log('Headers:', img_res.headers)
          console.log('Body', img_res.body)
        }
        throw new Error(`error on remote response (${img_res.statusCode})`)
      }

      const content_length = img_res.headers['content-length']
      if (content_length > config.max_size) {
        throw new Error(
          `Resource content_length exceeds limit (${content_length})`
        )
      }

      img = img_res.body

      let metadata
      let w = width
      let h = height

      if (w === 0) {
        w = null
      }

      if (h === 0) {
        h = null
      }

      if (width || height) {
        metadata = await sharp(img, { failOnError: true }).metadata()
        img = await sharp(img, { failOnError: true })
          .rotate()
          .resize(w, h, {
            fit: sharp.fit.inside,
            withoutEnlargement: true,
            position: 'center',
            background: { r: 0, g: 0, b: 0, alpha: 0 }
          })
          .toBuffer()
      }

      /// ///////
      content_type = img_res.headers['content-type']
      if (!accepted_content_types.includes(content_type.toLowerCase())) {
        throw new Error(`Unsupported content-type (${content_type})`)
      }

      if (
        content_type === 'binary/octet-stream' ||
        content_type === 'application/octet-stream'
      ) {
        content_type = `image/${metadata.format}`
      }

      if (!bypassCache) {
        await redisClient
          .multi()
          .hset(img_key, 'contentType', content_type)
          .hset(img_key, 'imgData', img)
          .expire(img_key, 86400) // keep the image cached in Redis for 24 hours.
          .exec()
      }
    }
    if (content_type) {
      res.set('content-type', content_type)
    }
    res.set('Cache-Control', 'public,max-age=29030400,immutable') // cache 48 weeks.

    res.end(img, 'binary')
  } catch (e) {
    console.log(e.message)

    res.set('content-type', 'image/png')
    res.set('Cache-Control', 'public,max-age=600,immutable') // cache 10 min.
    if (e.message === 'Blacklisted image') {
      res.sendFile('blocked.png', { root: assetsPath })
    } else {
      res.sendFile('error.png', { root: assetsPath })
    }
  } finally {
    // debug_sharp();
  }
})

router.get('/favicon.ico', async (req, res, next) => {
  res.set('content-type', 'image/x-icon')
  res.set('Cache-Control', 'public,max-age=209030400,immutable')
  res.sendFile('favicon.ico', { root: assetsPath })
})

router.get('/', async (req, res, next) => {
  res.set('Cache-Control', 'no-cache')
  res.status(200).json({ ok: true, version: '1.1.0', date: new Date() })
})

const debug_sharp = () => {
  const stats = sharp.cache()
  const threads = sharp.concurrency()
  const counters = sharp.counters()
  const simd = sharp.simd()

  console.log(JSON.stringify({ stats, threads, counters, simd }))
}

module.exports = router
