const config = require('config.json')('./config/config.json')
const needle = require('needle')
const fs = require('fs')

export const getJsonRpcUri = async (api, method, params) => {
  const jsonrpc = {
    jsonrpc: '2.0',
    method: `${api}.${method}`,
    params,
    id: 1
  }
  const api_res = await needle('post', config.rpc_url, jsonrpc, { json: true })
  const { body } = api_res
  return body.result
}

export const request_remote_image = (url) => {
  let logMessage = `Fecthing Remote Image - URL: ${url}`

  if (
    config.image_server_host !== '' &&
    url.search(config.image_server_host) > -1
  ) {
    url = url.replace('https', 'http')
    url = url.replace(
      config.image_server_host,
      config.image_server_private_host
    )
    logMessage = `Fetching Local Image - URL: ${url}`
  }

  console.log(logMessage)

  const options = {
    user_agent: 'Blurt-Image-Proxy/1.1.0',
    open_timeout: 60 * 1000,
    response_timeout: 0,
    read_timeout: 90 * 1000,
    compressed: true,
    parse_response: false,
    follow_max: 10
  }

  return new Promise((resolve, reject) => {
    needle.get(url, options, (error, response) => {
      if (error) {
        reject(error)
      } else {
        resolve(response)
      }
    })
  })
}

export let imageBlacklist
export const loadImageBlacklist = () => {
  const rawData = fs.readFileSync('./config/image_blacklist.json')
  imageBlacklist = JSON.parse(rawData)
  console.log('Loaded image blacklist.')
}

export const accepted_content_types = [
  'image/gif',
  'image/jpeg',
  'image/jpg',
  'image/png',
  'image/webp',
  'image/svg',
  'binary/octet-stream',
  'application/octet-stream'
]

export function emptyObject (value) {
  return Object.keys(value).length === 0
}
