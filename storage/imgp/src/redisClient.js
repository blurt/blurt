import Redis from 'ioredis'
const config = require('config.json')('./config/config.json')

export const redisClient = new Redis(config.redis_url)
