# RPC Nodes

RPC nodes are nodes on the blockchain that provide APIs that allow programs to interact with the Blurt blockchain. They allow reading information about the blockchain and broadcasting operations to the blockchain.

An RPC node can be private or public.

You can operate a private RPC node to supply your dapps and other programs built on Blurt. Running a local RPC node will make your interactions with the blockchain much faster.

A public RPC node will be reachable over the Internet so other developers can use them.

## RPC Node Hardware

**Infrastructure Providers**:
What's important here is that everyone is not using the same provider. We want the network to be as distributed as possible.

| **Provider**                | **Machine Types**    | **Price**   | **Special Feature**                                                                                                                                  |
| --------------------------- | -------------------- | ----------- | ---------------------------------------------------------------------------------------------------------------------------------------------------- |
| hetzner.de                  | Bare Metal and Cloud | Competitive | Cheap Bare Metal                                                                                                                                     |
| privex.io                   | Bare Metal and Cloud | Mid-range   | Privacy, Peering, Cryptocurrency Payments, Witness Optimization, Team has steemed since 2016                                                         |
| vultr.com                   | cloud and bare metal | Mid-Range   | Easy and straightforward                                                                                                                             |
| digitalocean.com            | cloud                | mid-range   | tutorial ecosystem                                                                                                                                   |
| contabo.com                 | cloud                | low         | Price AND one time I saw a contabo node outperform nearly all others in a network stress test situation on the [akash.io](https://akash.io) testnet. |
| Your local hosting provider | bare metal           | ?           | Diversify the Witness Set                                                                                                                            |
| GCE                         | Cloud                | high        | admin features                                                                                                                                       |
| AWS                         | Cloud                | high        | industry leader for infrastructure                                                                                                                   |

**Machine Spec**:
Your machine spec is entirely **your** choice. These recommended specs should be relatively low cost ($5-20 per month) and should also run your Blurt RPC node very effectively.

Accurate as of **February 20, 2022**:

**Blurt RPC Node Specs**

| Component   | Size     |
| ----------- | -------- |
| **CPU**     | 2+ Cores |
| **RAM**     | 4GB      |
| **Storage** | 160+ GB  |

## RPC Node Setup Procedure

**Valid for Mainnet, February 20, 2022:**

It is recommended to use either Debian 10 or Ubuntu 20.04.

### Configure Passwordless SSH Logins: IMPORTANT!

Make sure that you disable password logins on your machine and that you login to it ONLY using an SSH keypair. If you rent a machine with password logins enabled by default, no problem. Do like:

```bash
ssh-copy-id root@youripaddresshere
```

Enter your SSH password, and `ssh-copy-id` will copy your SSH public key to the server so that you no longer need to use a password to login.

Test this like:

```bash
ssh root@youripaddresshere
```

If it doesn't ask you for a password, you've been successful in setting up proper passwordless SSH that uses a signature by your SSH private key to authenticate you instead of a password. If it asks for a password, you've failed.

After you've SSH'd into your server (without it asking for a password) on your server, you should disable password-based logins like this:

```bash
nano /etc/ssh/sshd_config
```

Find this line:

```
#PasswordAuthentication yes
```

You should change it to:

```
PasswordAuthentication no
```

Press `Ctrl + o` to save the file, and `Ctrl + x` to exit the nano editor.

Then run

```bash
service ssh restart
service sshd restart
```

**DO NOT OPERATE A BLURT RPC NODE WITH PASSWORD AUTHENTICATION ENABLED.**

### Set up a Blurt RPC Node

Blurt nodes are now provided as presynced Docker container images with the latest build. This makes setting up and also upgrading during hardforks much quicker and smoother.

You will need to have root privileges to perform this installation. If you are not logged in as root, you can run the following command to get root privileges:

```bash
sudo su
```

Start by installing dependencies and getting your server up to date:

```bash
apt update
apt upgrade -y
apt install -y software-properties-common gnupg vnstat ifstat iftop atop htop  ufw fail2ban systemd-timesyncd
```

Install Docker:

```bash
curl -s https://get.docker.com | bash
```

Setup your firewall to block all incoming connections except for ssh and port 1776 for p2p connections:

```bash
ufw default deny
ufw allow ssh
ufw allow 1776
```

If you plan to run your RPC node as a public node, you will also need to open http and https ports. If you are running a private node, you can skip this step.

```bash
ufw allow http
ufw allow https
```

Then enable the firewall:

```bash
ufw enable
```

The Docker image is now presynced up to a certain date. This makes synchronization much quicker since you only have to synchronize the remaining blocks between the presynced blocks and the current block.

Start by pulling the image:

```bash
docker pull registry.gitlab.com/blurt/blurt/rpc-presync:dev
```

This will download the Docker image.

To install the Docker container with the Blurt node, run the following command:

```bash
docker run -d --net=host -v blurtd:/blurtd --name blurtrpc  registry.gitlab.com/blurt/blurt/rpc-presync:dev /usr/bin/blurtd --data-dir /blurtd --webserver-http-endpoint 0.0.0.0:8091 --webserver-ws-endpoint 0.0.0.0:8090 --p2p-endpoint 0.0.0.0:1776
```

Now, your Blurt node will start up inside of a Docker container.

You will have to wait for the node to synchronize the Blurt blockchain.

To monitor this process, you can run the following command:

```bash
docker logs blurtrpc -f
```

Once you see individual blocks being handled, the chain is fully synchronized.

It will look something like this:

```
1355794ms p2p_plugin.cpp:212            handle_block         ] Got 0 transactions on block 7561872 by saboin -- Block Time Offset: -205 ms
1358831ms p2p_plugin.cpp:212            handle_block         ] Got 0 transactions on block 7561873 by opfergnome -- Block Time Offset: -168 ms
1361695ms p2p_plugin.cpp:212            handle_block         ] Got 0 transactions on block 7561874 by blurthispano -- Block Time Offset: -304 ms
1364788ms p2p_plugin.cpp:212            handle_block         ] Got 0 transactions on block 7561875 by megadrive -- Block Time Offset: -212 ms
1367689ms p2p_plugin.cpp:212            handle_block         ] Got 0 transactions on block 7561876 by ionomy -- Block Time Offset: -310 ms
1370683ms p2p_plugin.cpp:212            handle_block         ] Got 1 transactions on block 7561877 by busbecq -- Block Time Offset: -316 ms
1374015ms p2p_plugin.cpp:212            handle_block         ] Got 0 transactions on block 7561878 by jakeminlim -- Block Time Offset: 15 ms
1376675ms p2p_plugin.cpp:212            handle_block         ] Got 0 transactions on block 7561879 by zahidsun -- Block Time Offset: -325 ms
1379685ms p2p_plugin.cpp:212            handle_block         ] Got 0 transactions on block 7561880 by imransoudagar -- Block Time Offset: -314 ms
1382709ms p2p_plugin.cpp:212            handle_block         ] Got 0 transactions on block 7561881 by double-u -- Block Time Offset: -290 ms
1385647ms p2p_plugin.cpp:212            handle_block         ] Got 0 transactions on block 7561882 by kentzz001 -- Block Time Offset: -352 ms
1388925ms p2p_plugin.cpp:212            handle_block         ] Got 0 transactions on block 7561883 by jacobgadikian -- Block Time Offset: -74 ms
1391731ms p2p_plugin.cpp:212            handle_block         ] Got 0 transactions on block 7561884 by empato365 -- Block Time Offset: -268 ms
1394757ms p2p_plugin.cpp:212            handle_block         ] Got 0 transactions on block 7561885 by africa.witness -- Block Time Offset: -242 ms
1397830ms p2p_plugin.cpp:212            handle_block         ] Got 0 transactions on block 7561886 by michelangelo3 -- Block Time Offset: -169 ms
```

Once you get there, you can exit the scrolling monitoring logs with `Ctrl + C`

If you are just installing the RPC for private use, you are done with the installation.

If you want to make it accessible to the public, continue to the next section.

## Installing Caddy as a Reverse Proxy

We recommend using Caddy as it is very easy to setup and use, and it will also automatically register SSL certificates for your domain so that your RPC uses the secure https protocol.

Before you install Caddy in order to make your node accessible to the public, you must first have a domain that you will use for it and point that domain's A and AAAA records to your server that is running the RPC node. It's important to have your domain pointed to your server before you start Caddy.

Start by installing Caddy:

##### NOTE: If you are not using a Debian-based OS, see [this page](https://caddyserver.com/docs/install) for installation instructionsfor your OS.

```bash
apt install -y debian-keyring debian-archive-keyring apt-transport-https
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/gpg.key' | sudo tee /etc/apt/trusted.gpg.d/caddy-stable.asc
curl -1sLf 'https://dl.cloudsmith.io/public/caddy/stable/debian.deb.txt' | sudo tee /etc/apt/sources.list.d/caddy-stable.list
apt update
apt install caddy
```

Next we're going to remove the existing Caddy configuration and put in our own.

Delete the existing Caddyfile:

```bash
rm /etc/caddy/Caddyfile
```

Create a blank Caddyfile:

```bash
touch /etc/caddy/Caddyfile
```

Then edit the Caddyfile:

```bash
nano /etc/caddy/Caddyfile
```

Paste in the following and replace `mydomain.com` with your actual domain:

```
mydomain.com {
    @options {
        method OPTIONS
    }
    encode zstd gzip
    header {
        Access-Control-Allow-Origin *
        Access-Control-Allow-Credentials true
        Access-Control-Allow-Methods *
        Access-Control-Allow-Headers *
        defer
    }
    reverse_proxy localhost:8091 {
        header_down -Access-Control-Allow-Origin
    }
    respond @options 204
}
```

Press `Ctrl + O` to save, then `Ctrl + X` to exit the nano editor.

## Accessing your Node

When you want to use your node locally, set your node URL in your software library to `http://localhost:8091` for http access, or `ws://localhost:8090` for websocket access.

To access it publicly over the Internet, set the URL to `https://mydomain.com`, replacing `mydomain.com` with your actual domain.

## Register on Ecosynthesizer

If you want to register your public node on Ecosynthesizer's website so that developers can be aware of it, go to [this page](https://ecosynthesizer.com/blurt/nodes), and click on "add" and register your node.

## Finished

Thank you so much for running blurt infrastructure. Blurt loves you!
