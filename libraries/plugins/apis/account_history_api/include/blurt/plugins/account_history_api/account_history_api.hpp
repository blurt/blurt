#pragma once
#include <blurt/plugins/json_rpc/utility.hpp>

#include <blurt/chain/history_object.hpp>

#include <blurt/protocol/operations.hpp>
#include <blurt/protocol/types.hpp>

#include <fc/optional.hpp>
#include <fc/variant.hpp>
#include <fc/vector.hpp>

namespace blurt { namespace plugins { namespace account_history {


namespace detail { class abstract_account_history_api_impl; }

struct api_operation_object
{
   api_operation_object() {}

   template< typename T >
   api_operation_object( const T& op_obj ) :
      trx_id( op_obj.trx_id ),
      block( op_obj.block ),
      trx_in_block( op_obj.trx_in_block ),
      virtual_op( op_obj.virtual_op ),
      timestamp( op_obj.timestamp )
   {
      op = fc::raw::unpack_from_buffer< blurt::protocol::operation >( op_obj.serialized_op );
   }

   blurt::protocol::transaction_id_type   trx_id;
   uint32_t                               block = 0;
   uint32_t                               trx_in_block = 0;
   uint32_t                               op_in_trx = 0;
   uint32_t                               virtual_op = 0;
   uint64_t                               operation_id = 0;
   fc::time_point_sec                     timestamp;
   blurt::protocol::operation             op;

   bool operator<( const api_operation_object& obj ) const
   {
      return std::tie( block, trx_in_block, op_in_trx, virtual_op ) < std::tie( obj.block, obj.trx_in_block, obj.op_in_trx, obj.virtual_op );
   }
};


struct get_ops_in_block_args
{
   uint32_t block_num;
   bool     only_virtual;
};

struct get_ops_in_block_return
{
   std::multiset< api_operation_object > ops;
};


struct get_transaction_args
{
   blurt::protocol::transaction_id_type id;
};

typedef blurt::protocol::annotated_signed_transaction get_transaction_return;


struct get_account_history_args
{
   blurt::protocol::account_name_type     account;
   uint64_t                               start = -1;
   uint32_t                               limit = 1000;

   /** if either are set, the set of returned operations will include only these
   * matching bitwise filter.
   * For the first 64 operations (as defined in protocol/operations.hpp), set the
   * corresponding bit in operation_filter_low; for the higher-numbered operations,
   * set the bit in operation_filter_high (pretending operation_filter is a
   * 128-bit bitmask composed of {operation_filter_high, operation_filter_low})
   */
   fc::optional<uint64_t> operation_filter_low;
   fc::optional<uint64_t> operation_filter_high;

};

struct get_account_history_return
{
   std::map< uint32_t, api_operation_object > history;
};

enum enum_vops_filter : uint64_t
{
   author_reward_operation                 = 0x0000'00000001ull,
   curation_reward_operation               = 0x0000'00000002ull,
   comment_reward_operation                = 0x0000'00000004ull,
   fill_vesting_withdraw_operation         = 0x0000'00000008ull,
   shutdown_witness_operation              = 0x0000'00000010ull,
   fill_transfer_from_savings_operation    = 0x0000'00000020ull,
   hardfork_operation                      = 0x0000'00000040ull,
   comment_payout_update_operation         = 0x0000'00000080ull,
   return_vesting_delegation_operation     = 0x0000'00000100ull,
   comment_benefactor_reward_operation     = 0x0000'00000200ull,
   producer_reward_operation               = 0x0000'00000400ull,
   clear_null_account_balance_operation    = 0x0000'00000800ull,
   proposal_pay_operation                  = 0x0000'00001000ull,
   sps_fund_operation                      = 0x0000'00002000ull,
   fee_pay_operation                       = 0x0000'00004000ull
};


/** Allows to specify range of blocks to retrieve virtual operations for.
 *  \param block_range_begin - starting block number (inclusive) to search for virtual operations
 *  \param block_range_end   - last block number (exclusive) to search for virtual operations
 *  \param operation_begin   - starting virtual operation in given block (inclusive)
 *  \param limit             - a limit of retrieved operations
 *  \param filter            - a filter that decides which an operation matches - used bitwise filtering equals to position in `hive::protocol::operation`
 */

struct enum_virtual_ops_args
{
   uint32_t block_range_begin = 1;
   uint32_t block_range_end = 2;
   fc::optional< uint64_t > operation_begin;
   fc::optional< int32_t > limit;
   fc::optional< uint64_t > filter;

};

struct enum_virtual_ops_return
{
   vector<api_operation_object> ops;
   uint32_t                     next_block_range_begin = 0;
   uint64_t                     next_operation_begin   = 0;
};


class account_history_api
{
   public:
      account_history_api();
      ~account_history_api();

      DECLARE_API(
         (get_ops_in_block)
         (get_transaction)
         (get_account_history)
         (enum_virtual_ops)
      )

   private:
      std::unique_ptr< detail::abstract_account_history_api_impl > my;
};

} } } // blurt::plugins::account_history

FC_REFLECT( blurt::plugins::account_history::api_operation_object,
   (trx_id)(block)(trx_in_block)(op_in_trx)(virtual_op)(timestamp)(op)(operation_id) )

FC_REFLECT( blurt::plugins::account_history::get_ops_in_block_args,
   (block_num)(only_virtual) )

FC_REFLECT( blurt::plugins::account_history::get_ops_in_block_return,
   (ops) )

FC_REFLECT( blurt::plugins::account_history::get_transaction_args,
   (id) )

FC_REFLECT( blurt::plugins::account_history::get_account_history_args,
   (account)(start)(limit)(operation_filter_low)(operation_filter_high) )

FC_REFLECT( blurt::plugins::account_history::get_account_history_return,
   (history) )

FC_REFLECT( blurt::plugins::account_history::enum_virtual_ops_args,
   (block_range_begin)(block_range_end)(operation_begin)(limit)(filter) )

FC_REFLECT( blurt::plugins::account_history::enum_virtual_ops_return,
   (ops)(next_block_range_begin)(next_operation_begin) )
