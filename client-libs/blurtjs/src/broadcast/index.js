import Promise from 'bluebird'
import newDebug from 'debug'

import broadcastHelpers from './helpers'
import { camelCase } from '../utils'
import { transaction as trxSerializer } from '../auth/serializer/src/operations'
import { hash } from '../auth/ecc'
import formatterFactory from '../formatter'
import operations from './operations'
import blurtApi from '../api'
import blurtAuth from '../auth'

const debug = newDebug('blurt:broadcast')
const noop = function () {}
const formatter = formatterFactory(blurtApi)

const blurtBroadcast = {}

// Base transaction logic -----------------------------------------------------

/**
 * Sign and broadcast transactions on the Blurt network
 */

blurtBroadcast.send = function blurtBroadcast$send (tx, privKeys, callback) {
  let trxId
  const resultP = blurtBroadcast
    ._prepareTransaction(tx)
    .then((transaction) => {
      debug(
        'Signing transaction (transaction, transaction.operations)',
        transaction,
        transaction.operations
      )

      const buf = trxSerializer.toBuffer(transaction)
      trxId = hash.sha256(buf).toString('hex').slice(0, 40)

      return Promise.join(
        transaction,
        blurtAuth.signTransaction(transaction, privKeys)
      )
    })
    .spread((transaction, signedTransaction) => {
      debug(
        'Broadcasting transaction (transaction, transaction.operations)',
        transaction,
        transaction.operations
      )
      return blurtApi
        .broadcastTransactionAsync(signedTransaction)
        .then((result) => {
          return Object.assign(
            { id: trxId },
            result,
            signedTransaction
          )
        })
    })

  resultP.nodeify(callback || noop)
}

blurtBroadcast._prepareTransaction =
    function blurtBroadcast$_prepareTransaction (tx) {
      const propertiesP = blurtApi.getDynamicGlobalPropertiesAsync()
      return propertiesP.then((properties) => {
        // Set defaults on the transaction
        const chainDate = new Date(properties.time + 'Z')
        const refBlockNum =
                (properties.last_irreversible_block_num - 1) & 0xffff
        return blurtApi
          .getBlockHeaderAsync(properties.last_irreversible_block_num)
          .then((block) => {
            const headBlockId = block
              ? block.previous
              : '0000000000000000000000000000000000000000'
            return Object.assign(
              {
                ref_block_num: refBlockNum,
                ref_block_prefix: Buffer.from(
                  headBlockId,
                  'hex'
                ).readUInt32LE(4),
                expiration: new Date(
                  chainDate.getTime() + 600 * 1000
                )
              },
              tx
            )
          })
      })
    }

// Generated wrapper ----------------------------------------------------------

// Generate operations from operations.json
operations.forEach((operation) => {
  const operationName = camelCase(operation.operation)
  const operationParams = operation.params || []

  const useCommentPermlink =
        operationParams.indexOf('parent_author') !== -1 &&
        operationParams.indexOf('parent_permlink') !== -1

  blurtBroadcast[`${operationName}With`] =
        function blurtBroadcast$specializedSendWith (wif, options, callback) {
          debug(`Sending operation "${operationName}" with`, {
            options,
            callback
          })
          const keys = {}
          if (operation.roles && operation.roles.length) {
            keys[operation.roles[0]] = wif // TODO - Automatically pick a role? Send all?
          }
          return blurtBroadcast.send(
            {
              extensions: [],
              operations: [
                [
                  operation.operation,
                  Object.assign(
                    {},
                    options,
                    options.json_metadata != null
                      ? {
                          json_metadata: toString(
                            options.json_metadata
                          )
                        }
                      : {},
                    useCommentPermlink && options.permlink == null
                      ? {
                          permlink: formatter.commentPermlink(
                            options.parent_author,
                            options.parent_permlink
                          )
                        }
                      : {}
                  )
                ]
              ]
            },
            keys,
            callback
          )
        }

  blurtBroadcast[operationName] = function blurtBroadcast$specializedSend (
    wif,
    ...args
  ) {
    debug(`Parsing operation "${operationName}" with`, { args })
    const options = operationParams.reduce((memo, param, i) => {
      memo[param] = args[i] // eslint-disable-line no-param-reassign
      return memo
    }, {})
    const callback = args[operationParams.length]
    return blurtBroadcast[`${operationName}With`](wif, options, callback)
  }
})

const toString = (obj) => (typeof obj === 'object' ? JSON.stringify(obj) : obj)
broadcastHelpers(blurtBroadcast)

Promise.promisifyAll(blurtBroadcast)

exports = module.exports = blurtBroadcast
