const blurt = require('../lib')

/* Generate private active WIF */
const username = process.env.BLURT_USERNAME
const password = process.env.BLURT_PASSWORD
const privActiveWif = blurt.auth.toWif(username, password, 'active')

/** Add posting account auth */
blurt.broadcast.addAccountAuth(
  {
    signingKey: privActiveWif,
    username,
    authorizedUsername: 'blurt.services',
    role: 'posting'
  },
  (err, result) => {
    console.log(err, result)
  }
)
