[![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/blurt/blurt/-/blob/dev/client-libs/blurtjs/LICENSE)

# Blurt.js

Blurt.js the Official JavaScript API for Blurt blockchain

# Documentation

-   [Install](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc#install)
-   [Browser](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc#browser)
-   [Config](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc#config)
-   [Database API](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc#api)
    -   [Subscriptions](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc#subscriptions)
    -   [Tags](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc#tags)
    -   [Blocks and transactions](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc#blocks-and-transactions)
    -   [Globals](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc#globals)
    -   [Keys](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc#keys)
    -   [Accounts](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc#accounts)
    -   [Authority / validation](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc#authority--validation)
    -   [Votes](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc#votes)
    -   [Content](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc#content)
    -   [Witnesses](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc#witnesses)
-   [Login API](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc#login)
-   [Follow API](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc#follow-api)
-   [Broadcast API](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc#broadcast-api)
-   [Broadcast](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc#broadcast)
-   [Auth](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc#auth)

Here is full documentation:
https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/doc

## Browser

```html
<script src="./blurt.min.js"></script>
<script>
    blurt.api.getAccounts(
        ["baabeetaa", "jacobgadikian"],
        function (err, response) {
            console.log(err, response);
        },
    );
</script>
```

## CDN

https://cdn.jsdelivr.net/npm/@blurtfoundation/blurtjs/dist/blurt.min.js<br/>

```html
<script src="https://cdn.jsdelivr.net/npm/@blurtfoundation/blurtjs/dist/blurt.min.js"></script>
```

## Webpack

[Please have a look at the webpack usage example.](https://gitlab.com/blurt/blurt/-/tree/dev/client-libs/blurtjs/examples/webpack-example)

## Server

## Install

```
$ npm install @blurtfoundation/blurtjs --save
```

## RPC Servers

https://rpc.blurt.world By Default<br/>
<sub>[List of Blurt nodes](https://ecosynthesizer.com/blurt/nodes)</sub><br/>

## Examples

### Broadcast Vote

```js
var blurt = require("@blurtfoundation/blurtjs");

var wif = blurt.auth.toWif(username, password, "posting");
blurt.broadcast.vote(
    wif,
    voter,
    author,
    permlink,
    weight,
    function (err, result) {
        console.log(err, result);
    },
);
```

### Get Accounts

```js
blurt.api.getAccounts(["megadrive", "jacobgadikian"], function (err, result) {
    console.log(err, result);
});
```

### Get State

```js
blurt.api.getState("/trends/funny", function (err, result) {
    console.log(err, result);
});
```

## Contributions

Patches are welcome! Contributors are listed in the package.json file. Please run the tests before opening a pull request and make sure that you are passing all of them.

## Issues

When you find issues, please report them!

## License

MIT
